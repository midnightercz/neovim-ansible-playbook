---
vim_plug_plugins:
  - nightsense/night-and-day
  - xolox/vim-colorscheme-switcher
  - avakhov/vim-yaml
  - Shougo/denite.nvim
  - Shougo/deoplete.nvim
  - zchee/deoplete-jedi
  - vim-scripts/yaml.vim
  #- airblade/vim-gitgutter
  - Chiel92/vim-autoformat
  - majutsushi/tagbar
  - MattesGroeger/vim-bookmarks
  - vimwiki/vimwiki
  - neomake/neomake
  - mklabs/split-term.vim
  - vim-airline/vim-airline
  - thaerkh/vim-workspace
  - ctrlpvim/ctrlp.vim
  #- tpope/vim-fugitive
  #- junegunn/gv.vim
  #- eugen0329/vim-esearch
  #- python-rope/ropevim 

vim_plug_colors:
  - AlessandroYorba/Despacio
  - muellan/am-colors
  - gosukiwi/vim-atom-dark
  - gwutz/vim-materialtheme
  - NLKNguyen/papercolor-theme
  - acarapetis/vim-colors-github
  - vim-airline/vim-airline-themes
  - zanglg/nova.vim
  - acarapetis/vim-colors-github
  - vim-scripts/moria

pathogen_plugins:
  - name: ansible-yaml
    source: https://github.com/chase/vim-ansible-yaml.git
    ref: master
  - name: nerdtree
    source: https://github.com/scrooloose/nerdtree.git
    ref: master
  - name: vim-javascript
    source: https://github.com/pangloss/vim-javascript.git
    ref: master

user_colorschemes:
  - name: molokai
    source: https://raw.githubusercontent.com/tomasr/molokai/master/colors/molokai.vim
  - name: kalisi
    source: https://raw.githubusercontent.com/freeo/vim-kalisi/master/colors/kalisi.vim


general_config:
  - ":syntax on"
  - ":colorscheme PaperColor"
  - ":set t_Co=256"
  - ":set mouse=a"
  - ":set colorcolumn=80"
  - ":set number"
  - ":set wrap"
  - ":set list"
  - ":set hidden"
  - "command GitAddThis :windo !git add %"
  - "command GitAmmend :windo !git commit --amend --no-edit"
  - "autocmd FileType javascript set tabstop=2|set shiftwidth=2|set expandtab"


plugin_configs:
  night_and_day:
    - "let g:nd_themes = [['07:00','PaperColor','light'],['16:00','despacio','dark'],['17:30','molokai','dark']]"
    #- "let g:nd_latitude = '50'"
  vim_gitgutter:
    - "let g:gitgutter_max_signs = 500"
    - "if &background == \"light\""
    - "  \"highlight GitGutterAddLine ctermbg=120 ctermfg=22"
    - "  highlight GitGutterAdd ctermbg=120 ctermfg=22"
    - "  \"highlight GitGutterRemoveLine ctermbg=203 ctermfg=88"
    - "  highlight GitGutterRemove ctermbg=203 ctermfg=88"
    - "  \"highlight GitGutterChangeLine ctermbg=245"
    - "  highlight GitGutterChange ctermbg=253"
    - "else"
    - "  \"highlight GitGutterAddLine ctermbg=120 ctermfg=22"
    - "  highlight GitGutterAdd ctermbg=120 ctermfg=22"
    - "  \"highlight GitGutterRemoveLine ctermbg=52 ctermfg=196"
    - "  highlight GitGutterRemove ctermbg=52 ctermfg=196"
    - "  \"highlight GitGutterChangeLine ctermbg=250"
    - "  highlight GitGutterChange ctermbg=250"
    - "endif"
    - "let g:gitgutter_highlight_lines = 1"
    - "let g:gitgutter_enabled = 1"
    - "let g:gitgutter_signs = 1"

  deoplete:
    - "let g:deoplete#enable_at_startup = 1"
  tagbar:
    - "autocmd FileType python | exe 'TagbarToggle'"
  autoformat:
    - "   let g:formatdef_autopep8 = '\"autopep8 - --ignore E371\".(g:DoesRangeEqualBuffer(a:firstline, a:lastline) ? \" --range \".a:firstline.\" \".a:lastline : \"\").\" \".(&textwidth ? \"--max-line-length=\".&textwidth : \"\")' "
    - "let g:formatters_python = ['yapf']"
    - "let g:formatter_yapf_style = 'pep8'"
      #  syntastic:
      #- "let g:syntastic_check_on_open = 1"
      #- "let g:syntastic_check_on_wq = 1"
      #- "let g:syntastic_python_checkers = ['flake8', 'mcabe']"
      #- "let g:syntastic_python_prospector_args = '-W pylint -F -s high'"
  arline:
    - 'let g:airline#extensions#tabline#enabled = 1'
    - 'map gr :bn<cr>'
    - 'map gR :bp<cr>'
    - 'map ;d :bd<cr>'
  neomake:
    - 'autocmd! BufWritePost,BufEnter * Neomake'
    - "let g:neomake_warning_sign = {'text': 'W','texthl': 'WarningMsg'}"
    - "let g:neomake_error_sign = {'text': 'E','texthl': 'ErrorMsg'}"
    - "let g:neomake_python_enable_makers = ['pylint']"
    - >
        let g:neomake_python_flake8_maker = {
        'args': ['--ignore=E221,E241,E272,E251,W702,E203,E201,E202',  '--format=default'],
        'errorformat': '%A%f: line %l\, col %v\, %m \ (%t%*\d\)',
        }
